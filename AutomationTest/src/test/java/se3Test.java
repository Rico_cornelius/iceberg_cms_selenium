import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by rico on 2/24/15.
 */
public class se3Test {

    WebDriver driver;
    WebDriverWait hold;
    WebElement button;
    ArrayList websites;
    String baseUrl, searchQuery;
    int index;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    public se3Test() {
        driver = new FirefoxDriver();
        websites = new ArrayList();
        hold = new WebDriverWait(driver, 10);
    }

    @Before
    public void setUp() throws Exception {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        baseUrl = "http://staging-se3.icehousecorp.com";
        searchQuery = "aaaa";
        websites.add("/#/movies/action");
        websites.add("/#/search?keywords=");
        websites.add("/#/userManagement/registration");
        websites.add("/#/movies/details/");
        index = 0;
    }

    @Test
    public void testLoadMain() throws Exception {
        //Open baseUrl - auto redirect to movies
        driver.get(baseUrl);
        Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase(baseUrl + websites.get(index)));
//        testHeader();

        //Use Search Function
        hold.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"right-navbar\"]/li/form/input")));
        driver.findElement(By.xpath("//*[@id=\"right-navbar\"]/li/form/input")).click();
        driver.findElement(By.xpath("//*[@id=\"right-navbar\"]/li/form/input")).sendKeys(searchQuery + Keys.ENTER);
        index=1;
        Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase(baseUrl + websites.get(index) + searchQuery));


        //Go to Movies
        button = driver.findElement(By.xpath("/html/body/div[4]/ul/li[2]/a"));
        Assert.assertTrue(button.getText().equals("Movies"));
        button.click();
        hold.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Action")));
        index = 0;
        Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase(baseUrl + websites.get(index)));
        testMovie();

        //Info Button on Featured Movie
        try {
            Assert.assertEquals("INFO", driver.findElement(By.xpath("(//a[contains(text(),'INFO')])[9]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
        driver.findElement(By.xpath("(//a[contains(text(),'INFO')])[9]")).click();
        index = 3;
        Assert.assertTrue(driver.getCurrentUrl().contains(baseUrl + websites.get(index)));
        testMovieDetail();

        //Go to register screen
        button = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.cssSelector("#right-navbar > a.ng-scope > li")));
        Assert.assertTrue(button.getText().equals("Register"));
        button.click();
        hold.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='user-registration-page']/div/ul/li/div[2]")));
        index = 2;
        Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase(baseUrl + websites.get(index)));

    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            Assert.fail(verificationErrorString);
        }
    }

    void testMovieDetail() {
//        Check movie cover is present
        try {
            hold.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img.img-responsive.img-thumbnail")));
            Assert.assertTrue(!driver.findElements(By.cssSelector("img.img-responsive.img-thumbnail")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check Title is present
        try {
            Assert.assertTrue(driver.findElement(By.cssSelector("header.movie-title-main.ng-binding")).getText().length()>0);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check Star Rate is exist
        try {
            Assert.assertTrue(!driver.findElements(By.className("star_rating")).isEmpty());
            System.out.println("Star Rate is " + driver.findElement(By.className("star_rating")).getAttribute("value"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check year
        try {
            Assert.assertTrue(!driver.findElements(By.cssSelector("li.ng-binding")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check Rating
        try {
            Assert.assertTrue(!driver.findElements(By.cssSelector("li.rating.ng-binding")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check Movie Length
        try {
            Assert.assertTrue(!driver.findElements(By.xpath("//div[@id='page-content']/div/ng-view/div/div/div/div[2]/div[2]/ul/li[4]")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check icon
        try {
            Assert.assertTrue(!driver.findElements(By.cssSelector("li.progress-circle > div")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check movie description
        try {
            Assert.assertTrue(!driver.findElements(By.cssSelector("p.movie-desc-details.ng-binding")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
//        Check Cast
        try {
            Assert.assertTrue(!driver.findElements(By.xpath("//div[@id='page-content']/div/ng-view/div/div/div/div[2]/div[2]/div/ul/li[2]")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

    }

    void testHeader(){
//        Check Eezing Logo (2 because the mobile resolution use different element)
        try {
//            hold.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img.img-responsive.img-thumbnail")));
            Assert.assertTrue(driver.findElements(By.className("image-logo")).size()==2);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/ul/li[3]/a")).getText().equals("TV"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/ul/li[2]/a")).getText().equals("Movies"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/ul/li[4]/a")).getText().equals("Recently Viewed"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/ul/li[5]/a")).getText().equals("Your Library"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[4]/ul/li[6]/a")).getText().equals("Wishlist"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"right-navbar\"]/a[1]/li")).getText().equals("Log In"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"right-navbar\"]/a[2]/li")).getText().equals("Register"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }

        try {
            Assert.assertTrue(!driver.findElements(By.xpath("//*[@id=\"right-navbar\"]/li/form/input")).isEmpty());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    void testMovie(){
//        Check genre bar

            System.out.println(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[1]/a")).getText());
            System.out.println(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[1]/a")).getAttribute("href"));
            Assert.assertTrue((driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[1]/a")).getAttribute("href")).contains(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[1]/a")).getText().toLowerCase()));

            System.out.println(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[2]/a")).getText());
            Assert.assertTrue((driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[2]/a")).getAttribute("href")).contains(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[2]/a")).getText().toLowerCase()));

            System.out.println(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[3]/a")).getText());
            Assert.assertTrue((driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[3]/a")).getAttribute("href")).contains(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[3]/a")).getText().toLowerCase()));

            System.out.println(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[4]/a")).getText());
            Assert.assertTrue((driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[4]/a")).getAttribute("href")).contains(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[4]/a")).getText().toLowerCase()));

        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[5]/a")).getText().length()>0);
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[5]/a")).getAttribute("aria-expanded").equals("false"));
        driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[5]/a")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"inner-scroll\"]/ul/li[5]/a")).getAttribute("aria-expanded").equals("true"));

    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}
