import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by rico on 2/12/15.
 */
public class SeleniumScript {

    static ArrayList websites;
    static WebDriver driver;

    public SeleniumScript() {
        websites = new ArrayList();
    }

    @BeforeClass
    public static void setup() {
        driver = new FirefoxDriver();
    }

    @Before
    public void setupTest(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        websites.add("https://iceberg-stage.icehousecorp.com/");
        websites.add("https://iceberg-stage.icehousecorp.com/auth/sign_in");
        websites.add("https://iceberg-stage.icehousecorp.com/auth/password/new");
    }

    @Test
    public void testaja(){
        driver.get((String) websites.get(0));
//        youtube(driver);
        login(driver);
    }

    @AfterClass
            public static void teardown(){
        driver.close();
    }

    public void youtube (WebDriver driver){
        driver.findElement(By.id("masthead-search-term")).sendKeys("aksdbfsdajfhb");
        driver.findElement(By.id("search-btn")).click();
        System.out.println(driver.findElement(By.tagName("strong")).getText());
//        System.out.println(driver.findElement(By.className("num-results first-focus")).getText());
    }

    Boolean login (WebDriver driver){
        Boolean result = true;
        System.out.println("Current address = "+driver.getCurrentUrl());
        WebElement test = new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(By.partialLinkText("Forgot")));
        test.click();

        System.out.println(this.testing(driver.getCurrentUrl(), 2));

        driver.findElement(By.linkText("Log in")).click();
        System.out.println(this.testing(driver.getCurrentUrl(), 1));
        driver.findElement(By.id("cms_user_email")).sendKeys("aaa@icehousecorp.com");
        driver.findElement(By.id("cms_user_password")).sendKeys("Password01");
        driver.findElement(By.name("commit")).click();
        System.out.println(this.testing(driver.getCurrentUrl(), 1));
        System.out.println("Alert = "+driver.findElement(By.className("alert-text")).getText());

        driver.findElement(By.id("cms_user_email")).sendKeys("admin@icehousecorp.com");
        driver.findElement(By.id("cms_user_password")).sendKeys("Password02");
        driver.findElement(By.name("commit")).click();
        System.out.println(this.testing(driver.getCurrentUrl(), 1));
        System.out.println("Alert = "+driver.findElement(By.className("alert-text")).getText());

        System.out.println("Remember me = "+driver.findElement(By.id("cms_user_remember_me")).isSelected());
        driver.findElement(By.id("cms_user_email")).sendKeys("admin@icehousecorp.com");
        driver.findElement(By.id("cms_user_password")).sendKeys("Password01");
        driver.findElement(By.name("commit")).click();
        System.out.println(this.testing(driver.getCurrentUrl(), 0));

        driver.findElement(By.linkText("Log out")).click();

        driver.findElement(By.id("cms_user_remember_me")).click();
        System.out.println("Remember me = "+driver.findElement(By.id("cms_user_remember_me")).isSelected());
        driver.findElement(By.id("cms_user_remember_me")).click();
        System.out.println("Remember me = "+driver.findElement(By.id("cms_user_remember_me")).isSelected());
        driver.findElement(By.id("cms_user_remember_me")).click();
        System.out.println("Remember me = "+driver.findElement(By.id("cms_user_remember_me")).isSelected());
        driver.findElement(By.id("cms_user_email")).sendKeys("admin@icehousecorp.com");
        driver.findElement(By.id("cms_user_password")).sendKeys("Password01");
        driver.findElement(By.name("commit")).click();
        return result;
    }

    String testing (String url, int target){
        return url.equalsIgnoreCase(websites.get(target).toString()) ? "Passed" : "Failed"+url;
    }

}
